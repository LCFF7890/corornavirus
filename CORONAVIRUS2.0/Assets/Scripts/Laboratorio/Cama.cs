﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cama : MonoBehaviour
{
    public Transform spawner;
    public GameObject text;
    public GameObject prueva;
    public GameObject canvasObject;
    private bool activar=true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Instantiate(text, spawner);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && Input.GetKeyDown(KeyCode.F) && activar == true)
        {
            activar = false;
            Debug.Log(false);
        }
        else
        {
            if (other.CompareTag("Player") && Input.GetKeyDown(KeyCode.F) && activar == false)
            {
                activar = true;
                Debug.Log(true);
            }
        }
        canvasObject.SetActive(activar);
    }
    private void OnTriggerExit(Collider other)
    {
        Destroy(GameObject.FindWithTag("text"));
        Destroy(GameObject.FindWithTag("Prueba"));
    }
}
