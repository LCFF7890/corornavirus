﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BandidoRangeAttack : MonoBehaviour
{
    public BandidoController bandido;

    public GameObject shoot;
    public GameObject spawnShoot;
    public bool canAttack;
    public float cooldownAttack;
    private float startCooldownAttck;

    // Start is called before the first frame update
    void Start()
    {
        canAttack = false;
        startCooldownAttck = 0;
    }

    // Update is called once per frame
    void Update()
    {
        nextShoot();
    }
    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Debug.Log("Bandio apuntando");
            transform.LookAt(other.transform, Vector3.up);
            if (canAttack)
            {
                Debug.Log("Bandido is attacking");
                attack();
            }
        }
    }
    private void nextShoot()
    {
        startCooldownAttck += Time.deltaTime;
        if (cooldownAttack < startCooldownAttck)
        {
            canAttack = true;
        }

    }
    private void attack()
    {
        Instantiate(shoot, spawnShoot.transform.position, spawnShoot.transform.rotation);        
        startCooldownAttck = 0;
        canAttack = false;        
    }
}
