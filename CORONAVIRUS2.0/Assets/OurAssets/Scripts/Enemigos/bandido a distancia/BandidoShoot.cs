﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BandidoShoot : MonoBehaviour
{
    public float shootSpeed;
    private Rigidbody _rigidbody;
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = this.gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        _rigidbody.AddRelativeForce(transform.forward);
    }
}
