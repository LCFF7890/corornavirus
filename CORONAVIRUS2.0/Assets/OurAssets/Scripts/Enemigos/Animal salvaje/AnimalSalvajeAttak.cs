﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalSalvajeAttak : MonoBehaviour
{
    public bool isAttaking;
    public float coolDownAttak;
    private float coolDownAttakTime;
    // Start is called before the first frame update
    void Start()
    {
        isAttaking = false;
        coolDownAttakTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        coolDownAttakTime += Time.deltaTime;
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player" && coolDownAttak <= coolDownAttakTime)
        {
            isAttaking = true;
            coolDownAttakTime = 0;
        }            
    }
}
