﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalSalvaje : MonoBehaviour
{
    public float speed;
    SphereCollider _rangeDetectionTrigger;
    public float rangeDetection;

    // Start is called before the first frame update
    void Start()
    {
        _rangeDetectionTrigger = this.gameObject.GetComponent<SphereCollider>();
        _rangeDetectionTrigger.radius = rangeDetection;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
         ///   transform.LookAt(other.transform, Vector3.up);
            Vector3 relativePos = other.transform.position - transform.position;
            transform.rotation = Quaternion.LookRotation(relativePos, Vector3.up);
            transform.Translate(relativePos * speed * Time.deltaTime, Space.World);
        }
    }

}
