﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuertaLaboratorio : MonoBehaviour
{
    private bool canEnter;
    public Text textInformation;
    Animator _animator;
    
    // Start is called before the first frame update
    void Start()
    {
        canEnter = false;       

        _animator = this.gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        animationDoor();
    }
    private void animationDoor()
    {
        if (canEnter)
        {
            _animator.SetBool("openingDoor", true);
        }
        else
        {
            _animator.SetBool("openingDoor", false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            canEnter = true;
            textInformation.text = "Pulsa F para entrar al Laboratorio";
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && Input.GetKeyDown(KeyCode.F))
        {
            
            // aqui se tendra que cambiar de escena a la del laboratorio
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            textInformation.text = "";
            canEnter = false;
        }
    }
}
