﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfectionBar : MonoBehaviour
{
    public Slider slider;

    public void setInfection(float currentInfection)
    {
        slider.value = currentInfection;
    }

    public void setMaxInfection(float maxInfection)
    {
        slider.maxValue = maxInfection;
        slider.value = 0f;
    }
}

