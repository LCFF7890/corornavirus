﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuitBar : MonoBehaviour
{
    public Slider slider;

    public void setSuitMaxHealth(float suitMaxHealth)
    {
        slider.maxValue = suitMaxHealth;
        slider.value = suitMaxHealth;
    }
    public void setSuitCurrentHealth(float suitCurrentHealth)
    {
        slider.value = suitCurrentHealth;
    }
}
