﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider slider;

    public void setMaxHealth(float maxLife)
    {
        slider.maxValue = maxLife;
        slider.value = maxLife;
    }
    public void setHealh(float currentHealth)
    {
        slider.value = currentHealth;
    }

}
